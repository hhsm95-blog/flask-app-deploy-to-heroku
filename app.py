import os

from dotenv import load_dotenv
from flask import Flask, jsonify


server = Flask(__name__)


@server.route("/api/ping", methods=['GET'])
def api_ping():
    return jsonify({"message": "pong"})


@server.route("/api/salute/<string:name>", methods=['GET'])
def api_salute(name: str):
    env = os.getenv("ENV")
    return jsonify({"message": f"Hi {name}!", "env": env})


if __name__ == "__main__":
    load_dotenv()
    server.run(host="localhost", port=8000, debug=True)
