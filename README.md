# Easily deploy your Python Flask web apps online with Gitlab and Heroku

Learn how easy is to deploy a Python Flask web app using Gitlab pipelines and Heroku when a new merge request is applied to master.

- Visit the original post: <https://hhsm95.dev/blog/easily-deploy-python-flask-app-with-gitlab-heroku/>

